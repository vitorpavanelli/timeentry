/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemple.service;

import exemple.service.HoraService;
import exemple.model.HoraType;
import exemple.model.Horario;
import exemple.model.HoraCalculada;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vitor
 */
public class HoraServiceTest {
    HoraService service = new HoraService();    
    
    public HoraServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void nehumaHoraExtraAndAtraso() throws Exception {
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_PADRAO)); 
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        assertTrue(service.getHorasCalculadas(lancamentos, horariosPadroes).isEmpty());
    } 
    
    @Test
    public void nehumaHoraExtraAndAtraso2() throws Exception {
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_PADRAO)); 
        horariosPadroes.add(new Horario("14:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("18:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("14:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("18:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
            
        
        assertTrue(service.getHorasCalculadas(lancamentos, horariosPadroes).isEmpty());
    }
    
    @Test
    public void nehumaHoraExtraAndAtraso3() throws Exception {
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_PADRAO)); 
        horariosPadroes.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_PADRAO)); 
        horariosPadroes.add(new Horario("14:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("18:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("14:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("18:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        assertTrue(service.getHorasCalculadas(lancamentos, horariosPadroes).isEmpty());
    } 
        
    @Test
    public void umaHoraExtraAndAtraso() throws Exception {
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_PADRAO)); 
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("07:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("11:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        int horaExtra = 0;
        int atraso  = 0;
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
             horaExtra += hc.getType() == HoraType.HORA_EXTRA ? 1 : 0;
             atraso += hc.getType() == HoraType.ATRASO ? 1 : 0;
        }            
        
        assertTrue(horaExtra == 1);
        assertTrue(atraso == 1);
    }
    
    
    @Test
    public void tresHorasExtrasPeriodo06a20() throws Exception {
        //06:00 a 20:00        
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_PADRAO)); 
        horariosPadroes.add(new Horario("13:30", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("17:30", HoraType.SAIDA, HoraType.HORA_PADRAO)); 
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("06:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("20:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
              
        
        int horaExtra = 0;        
        String[] horas = new String[3];
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
            horaExtra += hc.getType() == HoraType.HORA_EXTRA ? 1 : 0;
             
        }            
        
        assertTrue(horaExtra == 3);
        assertTrue("06:00 08:00".equals(horas[0]));
        assertTrue("12:00 13:30".equals(horas[1]));
        assertTrue("17:30 20:00".equals(horas[2]));
    }
    
    
    @Test
    public void periodo07a1230_14a17() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_PADRAO)); 
        horariosPadroes.add(new Horario("13:30", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("17:30", HoraType.SAIDA, HoraType.HORA_PADRAO)); 
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("07:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("12:30", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("14:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("17:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[2];
        String[] atrasos = new String[2];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 2);
        assertTrue("13:30 14:00".equals(atrasos[0]));
        assertTrue("17:00 17:30".equals(atrasos[1]));
        
        assertTrue(horaExtra == 2);
        assertTrue("07:00 08:00".equals(horas[0]));
        assertTrue("12:00 12:30".equals(horas[1]));        
    }
    
    @Test
    public void periodo21a04() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("05:00", HoraType.SAIDA, HoraType.HORA_PADRAO));         
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("21:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("04:00", HoraType.SAIDA, HoraType.HORA_LANCADA));        
        
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[1];
        String[] atrasos = new String[1];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 1);
        assertTrue("04:00 05:00".equals(atrasos[0]));
                
        assertTrue(horaExtra == 1);
        assertTrue("21:00 22:00".equals(horas[0]));        
    }
    
    @Test
    public void periodo03a07() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00        
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("05:00", HoraType.SAIDA, HoraType.HORA_PADRAO));         
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("03:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("07:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[1];
        String[] atrasos = new String[1];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 1);
        assertTrue("22:00 03:00".equals(atrasos[0]));
                
        assertTrue(horaExtra == 1);
        assertTrue("05:00 07:00".equals(horas[0]));        
    }
    
    
    /**
     * 
     * MODIFICAÇÕES A PARTIR DAQUI
     * TEST CASES DA RELATORIO ESTÃO A PARTIR DAQUI
     */
    @Test
    public void sim1_1() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_PADRAO));         
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("09:30", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("10:30", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[2];
        String[] atrasos = new String[2];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 1);
        assertTrue("09:30 10:30".equals(atrasos[0]));
        
        assertTrue(horaExtra == 0);        
    }
    
    @Test
    public void sim1_2() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_PADRAO));         
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("06:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("07:30", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("08:15", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("10:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("10:10", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("11:35", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("11:50", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("12:45", HoraType.SAIDA, HoraType.HORA_LANCADA));
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[2];
        String[] atrasos = new String[3];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 3);
        assertTrue("08:00 08:15".equals(atrasos[0]));
        assertTrue("10:00 10:10".equals(atrasos[1]));
        assertTrue("11:35 11:50".equals(atrasos[2]));
                
        assertTrue(horaExtra == 2);
        assertTrue("06:00 07:30".equals(horas[0])); 
        assertTrue("12:00 12:45".equals(horas[1]));
    }
    
    @Test
    public void sim2_1() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("14:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("18:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("11:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("12:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("16:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("17:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("21:00", HoraType.SAIDA, HoraType.HORA_LANCADA));        
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 2);
        assertTrue("11:00 12:00".equals(atrasos[0]));
        assertTrue("16:00 17:00".equals(atrasos[1]));        
                
        assertTrue(horaExtra == 2);
        assertTrue("12:00 14:00".equals(horas[0])); 
        assertTrue("18:00 21:00".equals(horas[1]));
    }
    
    @Test
    public void sim2_2() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("14:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("18:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("11:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("12:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("16:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("17:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("21:00", HoraType.SAIDA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_LANCADA)); 
        lancamentos.add(new Horario("23:00", HoraType.SAIDA, HoraType.HORA_LANCADA)); 
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 2);
        assertTrue("11:00 12:00".equals(atrasos[0]));
        assertTrue("16:00 17:00".equals(atrasos[1]));        
                
        assertTrue(horaExtra == 3);
        assertTrue("12:00 14:00".equals(horas[0])); 
        assertTrue("18:00 21:00".equals(horas[1]));
        assertTrue("22:00 23:00".equals(horas[2]));
    }
    
    @Test
    public void sim2_3() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("14:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("18:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("09:30", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("10:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("14:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("16:30", HoraType.SAIDA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("17:00", HoraType.ENTRADA, HoraType.HORA_LANCADA)); 
        lancamentos.add(new Horario("18:00", HoraType.SAIDA, HoraType.HORA_LANCADA)); 
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 2);
        assertTrue("09:30 10:00".equals(atrasos[0]));
        assertTrue("16:30 17:00".equals(atrasos[1]));        
                
        assertTrue(horaExtra == 0);        
    }
    
    @Test
    public void sim2_4() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("14:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("18:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("06:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("07:45", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        lancamentos.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("09:30", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        lancamentos.add(new Horario("10:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_LANCADA));        
        
        lancamentos.add(new Horario("14:00", HoraType.ENTRADA, HoraType.HORA_LANCADA)); 
        lancamentos.add(new Horario("16:30", HoraType.SAIDA, HoraType.HORA_LANCADA)); 
        
        lancamentos.add(new Horario("17:00", HoraType.ENTRADA, HoraType.HORA_LANCADA)); 
        lancamentos.add(new Horario("18:45", HoraType.SAIDA, HoraType.HORA_LANCADA)); 
        
        lancamentos.add(new Horario("19:55", HoraType.ENTRADA, HoraType.HORA_LANCADA)); 
        lancamentos.add(new Horario("21:50", HoraType.SAIDA, HoraType.HORA_LANCADA)); 
        
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 2);
        assertTrue("09:30 10:00".equals(atrasos[0]));
        assertTrue("16:30 17:00".equals(atrasos[1]));        
                
        assertTrue(horaExtra == 3);        
        assertTrue("06:00 07:45".equals(horas[0]));
        assertTrue("18:00 18:45".equals(horas[1]));
        assertTrue("19:55 21:50".equals(horas[2]));
    }
    
    @Test
    public void sim3_1() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("07:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("11:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("13:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("17:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("19:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("21:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("07:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("10:00", HoraType.SAIDA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("11:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("15:00", HoraType.SAIDA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("16:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("18:00", HoraType.SAIDA, HoraType.HORA_LANCADA));        
        
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 3);
        assertTrue("10:00 11:00".equals(atrasos[0]));
        assertTrue("15:00 16:00".equals(atrasos[1]));        
        assertTrue("19:00 21:00".equals(atrasos[2]));
                
        assertTrue(horaExtra == 2);        
        assertTrue("11:00 13:00".equals(horas[0]));
        assertTrue("17:00 18:00".equals(horas[1]));        
    }
    
    @Test
    public void sim4_1() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("23:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 1);
        assertTrue("22:00 23:00".equals(atrasos[0]));
                
        assertTrue(horaExtra == 0);                
    }
    
    @Test
    public void sim4_2() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("23:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("05:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 2);
        assertTrue("22:00 23:00".equals(atrasos[0]));
        assertTrue("05:00 06:00".equals(atrasos[1]));
                
        assertTrue(horaExtra == 0);                
    }
    
    @Test
    public void sim4_3() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("01:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("02:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 1);
        assertTrue("01:00 02:00".equals(atrasos[0]));        
                
        assertTrue(horaExtra == 0);                
    }
    
    @Test
    public void sim4_4() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("20:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("22:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("06:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("07:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 1);
        assertTrue("22:00 06:00".equals(atrasos[0]));        
        
        assertTrue(horaExtra == 2);
        assertTrue("20:00 22:00".equals(horas[0]));
        assertTrue("06:00 07:00".equals(horas[1]));
    }
    
    @Test
    public void sim5_1() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("01:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        horariosPadroes.add(new Horario("02:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("01:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("02:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 0);        
        assertTrue(horaExtra == 0);        
    }
    
    @Test
    public void sim5_2() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("01:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        horariosPadroes.add(new Horario("02:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("21:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("00:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("01:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("05:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 2);
        assertTrue("00:00 01:00".equals(atrasos[0]));        
        assertTrue("05:00 06:00".equals(atrasos[1]));
        
        assertTrue(horaExtra == 2);
        assertTrue("21:00 22:00".equals(horas[0]));
        assertTrue("01:00 02:00".equals(horas[1]));
    }
    
    @Test
    public void sim5_3() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("01:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        horariosPadroes.add(new Horario("02:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("23:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("02:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("03:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("07:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 2);
        assertTrue("22:00 23:00".equals(atrasos[0]));        
        assertTrue("02:00 03:00".equals(atrasos[1]));
        
        assertTrue(horaExtra == 2);
        assertTrue("01:00 02:00".equals(horas[0]));
        assertTrue("06:00 07:00".equals(horas[1]));
    }
    
    @Test
    public void sim5_4() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("01:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        horariosPadroes.add(new Horario("02:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("01:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("07:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("15:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 1);
        assertTrue("02:00 06:00".equals(atrasos[0]));                
        
        assertTrue(horaExtra == 1);
        assertTrue("07:00 15:00".equals(horas[0]));        
    }    
    
    @Test
    public void sim5_5() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("01:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        horariosPadroes.add(new Horario("02:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("20:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("21:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("02:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 1);
        assertTrue("22:00 01:00".equals(atrasos[0]));                
        
        assertTrue(horaExtra == 1);
        assertTrue("20:00 21:00".equals(horas[0]));        
    } 
    
    @Test
    public void sim5_6() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("01:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        horariosPadroes.add(new Horario("02:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("00:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("01:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("04:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("05:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("10:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 2);
        assertTrue("00:00 01:00".equals(atrasos[0]));                
        assertTrue("04:00 05:00".equals(atrasos[1]));  
        
        assertTrue(horaExtra == 2);
        assertTrue("01:00 02:00".equals(horas[0]));        
        assertTrue("06:00 10:00".equals(horas[1]));
    } 
    
    @Test
    public void sim5_7() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("01:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        horariosPadroes.add(new Horario("02:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_PADRAO));    
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("01:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("02:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("07:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("10:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 0);
                
        assertTrue(horaExtra == 1);
        assertTrue("07:00 10:00".equals(horas[0]));                
    }
    
    /**
     * 
     * 04/09/2017 - novo relatório de erros
     */
    @Test
    public void sim1_1_1() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_PADRAO));         
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("13:00", HoraType.SAIDA, HoraType.HORA_LANCADA));        
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[2];
        String[] atrasos = new String[2];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 0);
                
        assertTrue(horaExtra == 1);
        assertTrue("12:00 13:00".equals(horas[0]));
    }
    
    @Test
    public void sim1_1_2() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_PADRAO));         
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("06:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("08:00", HoraType.SAIDA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("12:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("13:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[2];
        String[] atrasos = new String[2];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 1);
        assertTrue("08:00 12:00".equals(atrasos[0]));        
        
        assertTrue(horaExtra == 2);
        assertTrue("06:00 08:00".equals(horas[0]));
        assertTrue("12:00 13:00".equals(horas[1]));
    }
    
    @Test
    public void sim2_1_1() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("14:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("18:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));                
        lancamentos.add(new Horario("17:00", HoraType.SAIDA, HoraType.HORA_LANCADA));        
              
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 1);
        assertTrue("17:00 18:00".equals(atrasos[0]));        
                
        assertTrue(horaExtra == 1);
        assertTrue("12:00 14:00".equals(horas[0]));         
    }
    
    @Test
    public void sim2_1_2() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("14:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("18:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("06:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("07:45", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("09:30", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("10:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("12:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("14:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("16:30", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("17:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("18:45", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("19:55", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("21:50", HoraType.SAIDA, HoraType.HORA_LANCADA));
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 2);
        assertTrue("09:30 10:00".equals(atrasos[0]));
        assertTrue("16:30 17:00".equals(atrasos[1]));        
                
        assertTrue(horaExtra == 3);
        assertTrue("06:00 07:45".equals(horas[0])); 
        assertTrue("18:00 18:45".equals(horas[1]));
        assertTrue("19:55 21:50".equals(horas[2]));
    }
    
     @Test
    public void sim3_1_1() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("07:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("11:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("13:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("17:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("19:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("21:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("06:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("11:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("12:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("17:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("18:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("21:00", HoraType.SAIDA, HoraType.HORA_LANCADA));        
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 0);        
                
        assertTrue(horaExtra == 3);
        assertTrue("06:00 07:00".equals(horas[0])); 
        assertTrue("12:00 13:00".equals(horas[1]));
        assertTrue("18:00 19:00".equals(horas[2]));
    }
    
    @Test
    public void sim3_1_2() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("07:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("11:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("13:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("17:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("19:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("21:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("08:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("11:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("14:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("17:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("20:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("21:00", HoraType.SAIDA, HoraType.HORA_LANCADA));        
        
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 3);
        assertTrue("07:00 08:00".equals(atrasos[0]));
        assertTrue("13:00 14:00".equals(atrasos[1]));        
        assertTrue("19:00 20:00".equals(atrasos[2]));
                
        assertTrue(horaExtra == 0);        
    }
    
    @Test
    public void sim3_1_3() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("07:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("11:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("13:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("17:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("19:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("21:00", HoraType.SAIDA, HoraType.HORA_PADRAO));
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("11:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("13:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
        lancamentos.add(new Horario("17:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("19:00", HoraType.SAIDA, HoraType.HORA_LANCADA));              
        
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 3);
        assertTrue("07:00 11:00".equals(atrasos[0]));
        assertTrue("13:00 17:00".equals(atrasos[1]));        
        assertTrue("19:00 21:00".equals(atrasos[2]));
                
        assertTrue(horaExtra == 2);        
        assertTrue("11:00 13:00".equals(horas[0]));
        assertTrue("17:00 19:00".equals(horas[1]));                
    }
    
    @Test
    public void sim4_1_1() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_PADRAO));        
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("21:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 0);        
                
        assertTrue(horaExtra == 1);        
        assertTrue("21:00 22:00".equals(horas[0]));
    }
    
    @Test
    public void sim4_1_2() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_PADRAO));        
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("07:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 0);        
                
        assertTrue(horaExtra == 1);        
        assertTrue("06:00 07:00".equals(horas[0]));                    
    }
    
    @Test
    public void sim4_1_3() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_PADRAO));        
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("21:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("07:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 0);        
                
        assertTrue(horaExtra == 2);        
        assertTrue("21:00 22:00".equals(horas[0]));
        assertTrue("06:00 07:00".equals(horas[1]));                
    }
    
    @Test
    public void sim5_1_1() throws Exception {
        //07:00 - 12:30 e 14:00 a 17:00
        List<Horario> horariosPadroes = new ArrayList();        
        horariosPadroes.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("01:00", HoraType.SAIDA, HoraType.HORA_PADRAO));        
        horariosPadroes.add(new Horario("02:00", HoraType.ENTRADA, HoraType.HORA_PADRAO));
        horariosPadroes.add(new Horario("06:00", HoraType.SAIDA, HoraType.HORA_PADRAO));        
        
        List<Horario> lancamentos = new ArrayList();
        lancamentos.add(new Horario("22:00", HoraType.ENTRADA, HoraType.HORA_LANCADA));        
        lancamentos.add(new Horario("05:00", HoraType.SAIDA, HoraType.HORA_LANCADA));
                      
        int horaExtra = 0;        
        int atraso = 0; 
        String[] horas = new String[10];
        String[] atrasos = new String[10];
        
        for (HoraCalculada hc: service.getHorasCalculadas(lancamentos, horariosPadroes)) {
            if (hc.getType() == HoraType.HORA_EXTRA) {
                horas[horaExtra] = hc.getFrom() + " " + hc.getTo();
                horaExtra += 1;
            }
            
            if (hc.getType() == HoraType.ATRASO) {
                atrasos[atraso] = hc.getFrom() + " " + hc.getTo();
                atraso += 1;
            }
        }            
                
        assertTrue(atraso == 1);        
        assertTrue("05:00 06:00".equals(atrasos[0]));                        
        
        assertTrue(horaExtra == 1);        
        assertTrue("01:00 02:00".equals(horas[0]));
        
    }
}
