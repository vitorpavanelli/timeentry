/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemple.model;

/**
 *
 * @author vitor
 */
public enum HoraType {
    HORA_EXTRA,
    ATRASO,
    ENTRADA,
    SAIDA,
    HORA_PADRAO,
    HORA_LANCADA
}
