/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemple.model;

/**
 *
 * @author vitor
 */
public class Horario {
    
    private String horaText;
    
    private HoraType saidaEntrada;
    
    private HoraType padraoLancada;
    
    public Horario() {
        
    }
    
    public Horario(String horaText, HoraType saidaEntrada, HoraType padraoLancada) {
        this.horaText = horaText;
        this.saidaEntrada = saidaEntrada;
        this.padraoLancada = padraoLancada;
    }

    public String getHoraText() {
        return horaText;
    }

    public void setHoraText(String horaText) {
        this.horaText = horaText;
    }
    
    public int getHora() {
        String[] s = horaText.split(":");
        return Integer.parseInt(s[0] + s[1]);
    }

    public HoraType getSaidaEntrada() {
        return saidaEntrada;
    }

    public void setSaidaEntrada(HoraType saidaEntrada) {
        this.saidaEntrada = saidaEntrada;
    }

    public HoraType getPadraoLancada() {
        return padraoLancada;
    }

    public void setPadraoLancada(HoraType padraoLancada) {
        this.padraoLancada = padraoLancada;
    }
    
    
}
