/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemple.service;

import exemple.model.HoraCalculada;
import exemple.model.HoraType;
import exemple.model.Horario;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vitor
 */
public class HoraService {

    public static final int ENTRADA = 0;
    public static final int SAIDA = 1;
   
    public List<HoraCalculada> getHorasCalculadas(List<Horario> lancamentos, List<Horario> horariosPadroes) throws HoraException {
        if (lancamentos == null || lancamentos.isEmpty()) {
            throw new HoraException("Não existem marcações");
        }

        if (horariosPadroes == null || horariosPadroes.isEmpty()) {
            throw new HoraException("Não existem horários de trabalho");
        }

        List<Horario> noiteDiaPeriod = getNoiteDiaPeriod(horariosPadroes);
        List<HoraCalculada> horasCalculadas = new ArrayList();
        int pos = 0;
        int i = 0;
        for (int index = 0; index < horariosPadroes.size(); index += 2) {
            Horario entrada = horariosPadroes.get(index);
            Horario saida = horariosPadroes.get(index + 1);

            boolean isNoiteDia = false;            
            if (noiteDiaPeriod != null) {
                isNoiteDia = entrada.getHora() == noiteDiaPeriod.get(ENTRADA).getHora();
            }
            
            // filtramos todos os items para um período
            List<Horario> filteredLancamentos = new ArrayList();
            if (!isNoiteDia) {
                for (i = pos; i < lancamentos.size(); i++) {
                    Horario l = lancamentos.get(i);
                    if (l.getHora() < saida.getHora()) {
                        filteredLancamentos.add(l);
                        pos++;
                    }
                }
            } else {
                if (horariosPadroes.size() > 2) {
                    Horario nextEntrada = horariosPadroes.get(index + 2);                                      
                    Horario nextSaida = horariosPadroes.get(index + 3);
                    int lastLower = 0;
                    for (int j = 0; j < lancamentos.size(); j++) {
                        Horario l = lancamentos.get(j);
                        if (l.getHora() == nextEntrada.getHora() && l.getSaidaEntrada() == HoraType.ENTRADA) {
                            lastLower = j;
                            break;
                        }
                        
                        if (l.getHora() < nextSaida.getHora()) {
                            if (l.getHora() == saida.getHora()) {
                                lastLower = j;
                                break;
                            }
                            
                            if (l.getHora() >= nextEntrada.getHora() && l.getSaidaEntrada() == HoraType.SAIDA) {
                                lastLower = j;
                                if (l.getHora() == nextEntrada.getHora()) {
                                    lastLower++;
                                }
                                break;
                            }
                        }
                    }
                    
                    for (i = 0; i < lastLower; i++) {
                        Horario l = lancamentos.get(i);
                        filteredLancamentos.add(l);
                        pos++;
                    }
                }
            }
            
            // se for último período, pegamos todas a entries após a última
            // saída padrão
            if (index == (horariosPadroes.size() - 2)) {
                for (int j = pos; j < lancamentos.size(); j++) {
                    filteredLancamentos.add(lancamentos.get(j));
                }

            } else if (pos <= (lancamentos.size() - 1)) {
                // se há alguma entry após uma saída, precisamos adicionar
                Horario h = lancamentos.get(pos);
                if (h.getSaidaEntrada() == HoraType.SAIDA) {
                    if (index < (horariosPadroes.size() - 2)) {
                        Horario proxEntrada = horariosPadroes.get(index + 2);
                        if (h.getHora() < proxEntrada.getHora()) {
                            filteredLancamentos.add(h);
                            pos++;
                        }

                    } else {
                        filteredLancamentos.add(h);
                        pos++;
                    }
                }
            }
            
            if (filteredLancamentos.size() == 1) {
                // precisamos adicionar uma entry caso o horário de saída e entrada 
                // ocupem mais de um período
                if (filteredLancamentos.get(0).getSaidaEntrada() == HoraType.SAIDA) {
                    Horario h = new Horario(horariosPadroes.get(index - 1).getHoraText(), HoraType.ENTRADA, HoraType.HORA_LANCADA);
                    filteredLancamentos.add(0, h);
                }
            } 
                        
            horasCalculadas.addAll(calculateHoras(filteredLancamentos, lancamentos, entrada, saida, isNoiteDia));            
        }

        return horasCalculadas;
    }
    
    private List<Horario> getNoiteDiaPeriod(List<Horario> horariosPadroes) {
        for (int index = 0; index < horariosPadroes.size(); index++) {
            Horario horaEntrada = horariosPadroes.get(index);
            Horario horaSaida = horariosPadroes.get(index + 1);
            if (horaSaida.getHora() < horaEntrada.getHora()) {
                List<Horario> h = new ArrayList();
                h.add(horaEntrada);
                h.add(horaSaida);
                return h;
            }
            index++;
        }
        return null;
    }

    private HoraCalculada getHoraCalculada(String from, String to, HoraType type) {
        return new HoraCalculada(from, to, type);
    }
    
    private List<HoraCalculada> calculateHoras(List<Horario> filteredLancamentos, 
            List<Horario> lancamentos, Horario entrada, Horario saida, boolean isNoiteDia) {        
        List<HoraCalculada> horasCalculadas = new ArrayList();

        if (filteredLancamentos.isEmpty()) {
            // se não temos entries, então pode ser que todos os lançamentos
            // tenham sido realizados
            boolean hasLancamento = false;            
            for (Horario h: lancamentos) {
                if (h.getHora() == entrada.getHora()) {
                    hasLancamento = true;                    
                    break;
                }         
            }
            
            // se não foi realizado o lançamento, então atraso
            if (!hasLancamento) {
                horasCalculadas.add(getHoraCalculada(entrada.getHoraText(),
                    saida.getHoraText(), HoraType.ATRASO));
            }
        
        } else if (isNoiteDia) {
            horasCalculadas = calculateHorasNoiteDia(filteredLancamentos, entrada, saida);
        
        } else if (filteredLancamentos.get(0).getHora() >= saida.getHora()) {
            // se o primeiro lançamento é maior que a saída, significar que
            // podemos apenas considerar período como atraso e tudo após como extra
            if (filteredLancamentos.get(0).getSaidaEntrada() == HoraType.ENTRADA) {
                horasCalculadas.add(getHoraCalculada(entrada.getHoraText(), saida.getHoraText(), HoraType.ATRASO));
            }
            
            for (int index = 0; index < filteredLancamentos.size(); index += 2) {
                if (index + 1 < filteredLancamentos.size()) {
                    horasCalculadas.add(getHoraCalculada(filteredLancamentos.get(index).getHoraText(),
                            filteredLancamentos.get(index + 1).getHoraText(), HoraType.HORA_EXTRA));

                } else {
                    horasCalculadas.add(getHoraCalculada(saida.getHoraText(),
                            filteredLancamentos.get(index).getHoraText(), HoraType.HORA_EXTRA));
                }
            }
        
        } else {
            int horasIndex = -1;
            HoraCalculada horaCalculada = null;
            for (int index = 0; index < filteredLancamentos.size(); index++) {
                Horario lancamento = filteredLancamentos.get(index);
                if (horasCalculadas.size() > 0) {
                    horaCalculada = horasCalculadas.get(horasIndex);
                }

                if (lancamento.getSaidaEntrada() == HoraType.ENTRADA) {
                    if (lancamento.getHora() != entrada.getHora()) {
                        if (horaCalculada == null) {
                            if (lancamento.getHora() < entrada.getHora()) {
                                horasCalculadas.add(getHoraCalculada(lancamento.getHoraText(), entrada.getHoraText(), HoraType.HORA_EXTRA));

                            } else if (lancamento.getHora() > saida.getHora()) {
                                // todos os lançamentos após saída padrão devem ser considerados hora extra
                                for (int j = index; j < filteredLancamentos.size(); j += 2) {
                                    horasCalculadas.add(getHoraCalculada(filteredLancamentos.get(j).getHoraText(), 
                                            filteredLancamentos.get(j + 1).getHoraText(), HoraType.HORA_EXTRA));                                        
                                }
                                break;

                            } else {
                                horasCalculadas.add(getHoraCalculada(entrada.getHoraText(), lancamento.getHoraText(), HoraType.ATRASO));
                            }

                            horasIndex++;

                        } else if (lancamento.getHora() < horaCalculada.getHoraTo()) {
                                horaCalculada.setTo(lancamento.getHoraText());
                                horasCalculadas.set(horasIndex, horaCalculada);

                        } else if (lancamento.getHora() > saida.getHora()) {
                            // todos os lançamentos após saída padrão devem ser considerados hora extra
                            for (int j = index; j < filteredLancamentos.size(); j += 2) {
                                horasCalculadas.add(getHoraCalculada(filteredLancamentos.get(j).getHoraText(),
                                        filteredLancamentos.get(j + 1).getHoraText(), HoraType.HORA_EXTRA));
                            }
                            break;

                        } else if (lancamento.getHora() < entrada.getHora()) {
                            horasCalculadas.add(getHoraCalculada(lancamento.getHoraText(), entrada.getHoraText(), HoraType.HORA_EXTRA));
                            horasIndex++;

                        } else if (lancamento.getHora() < saida.getHora()) {
                            horasCalculadas.add(getHoraCalculada(entrada.getHoraText(), lancamento.getHoraText(), HoraType.ATRASO));
                            horasIndex++;
                        }
                    }
                }

                if (lancamento.getSaidaEntrada() == HoraType.SAIDA) {
                    if (lancamento.getHora() != saida.getHora()) {
                        if (horaCalculada == null) {
                            if (lancamento.getHora() < saida.getHora()) {
                                horasCalculadas.add(getHoraCalculada(lancamento.getHoraText(), saida.getHoraText(), HoraType.ATRASO));

                            } else {
                                horasCalculadas.add(getHoraCalculada(saida.getHoraText(), lancamento.getHoraText(), HoraType.HORA_EXTRA));
                            }

                            horasIndex++;

                        } else if (lancamento.getHora() < horaCalculada.getHoraTo()) {
                                horaCalculada.setTo(lancamento.getHoraText());
                                horasCalculadas.set(horasIndex, horaCalculada);

                        } else if (lancamento.getHora() < saida.getHora()) {
                            horasCalculadas.add(getHoraCalculada(lancamento.getHoraText(), saida.getHoraText(), HoraType.ATRASO));
                            horasIndex++;

                        } else {
                            horasCalculadas.add(getHoraCalculada(saida.getHoraText(), lancamento.getHoraText(), HoraType.HORA_EXTRA));
                            horasIndex++;
                        }
                    }
                }
            }
        }        

        return horasCalculadas;
    }
    
    private List<HoraCalculada> calculateHorasNoiteDia(List<Horario> filteredLancamentos, Horario entrada, Horario saida) {        
        // temos lançamentos entre os períodos
        boolean hasLowerThanSaida = false;
        boolean hasGreaterThanEntrada = false;
        for (int index = 0; index < filteredLancamentos.size(); index++) {
            Horario lancamento = filteredLancamentos.get(index);            
            if (lancamento.getHora() < saida.getHora()){
                hasLowerThanSaida = true;
            }            
            
            if (lancamento.getHora() > entrada.getHora()){
                hasGreaterThanEntrada = true;
            }
        }
        
        List<HoraCalculada> horasCalculadas = new ArrayList();
        int horasIndex = -1;
        HoraCalculada horaCalculada = null;
        for (int index = 0; index < filteredLancamentos.size(); index++) {
            Horario lancamento = filteredLancamentos.get(index);
            if (horasCalculadas.size() > 0) {
                horaCalculada = horasCalculadas.get(horasIndex);
            }

            if (lancamento.getSaidaEntrada() == HoraType.ENTRADA) {
                if (lancamento.getHora() != entrada.getHora()) {
                    if (horaCalculada == null) {
                        if (lancamento.getHora() < entrada.getHora() && lancamento.getHora() > saida.getHora()) {
                            horasCalculadas.add(getHoraCalculada(lancamento.getHoraText(), entrada.getHoraText(), HoraType.HORA_EXTRA));

                        } else {
                            horasCalculadas.add(getHoraCalculada(entrada.getHoraText(), lancamento.getHoraText(), HoraType.ATRASO));
                        }

                        horasIndex++;

                    } else {
                        if (lancamento.getHora() < horaCalculada.getHoraTo()) {
                            horaCalculada.setTo(lancamento.getHoraText());
                            horasCalculadas.set(horasIndex, horaCalculada);                        

                        } else if (lancamento.getHora() < entrada.getHora()) {
                            horasCalculadas.add(getHoraCalculada(lancamento.getHoraText(), entrada.getHoraText(), HoraType.HORA_EXTRA));
                            horasIndex++;
                        }
                    }
                }
            }

            if (lancamento.getSaidaEntrada() == HoraType.SAIDA) {
                if (lancamento.getHora() != saida.getHora()) {
                    if (horaCalculada == null) {
                        if (lancamento.getHora() < saida.getHora()) {
                            horasCalculadas.add(getHoraCalculada(lancamento.getHoraText(), saida.getHoraText(), HoraType.ATRASO));
                        
                        } else if (lancamento.getHora() > saida.getHora()) {
                            horasCalculadas.add(getHoraCalculada(saida.getHoraText(), lancamento.getHoraText(), HoraType.HORA_EXTRA));
                        }

                        horasIndex++;

                    } else {
                        if (lancamento.getHora() > saida.getHora()) {
                             if (lancamento.getHora() < horaCalculada.getHoraTo() && lancamento.getHora() > horaCalculada.getHoraFrom()) {
                                horaCalculada.setTo(lancamento.getHoraText());
                                horasCalculadas.set(horasIndex, horaCalculada);                                
                                
                                if (!hasLowerThanSaida && !hasGreaterThanEntrada) {
                                    horasCalculadas.add(getHoraCalculada(entrada.getHoraText(), saida.getHoraText(), HoraType.ATRASO));                                    
                                }

                             } else if (lancamento.getHora() >= entrada.getHora()) {
                                horasCalculadas.add(getHoraCalculada(lancamento.getHoraText(), saida.getHoraText(), HoraType.ATRASO));
                                horasIndex++; 
                                
                                if (lancamento.getHora() == entrada.getHora()) {
                                    hasLowerThanSaida = true;
                                    hasGreaterThanEntrada = true;
                                }
                             
                             } else {
                                horasCalculadas.add(getHoraCalculada(saida.getHoraText(), lancamento.getHoraText(), HoraType.HORA_EXTRA));
                                horasIndex++;
                             }

                        } else if (lancamento.getHora() < saida.getHora()) {
                            horasCalculadas.add(getHoraCalculada(lancamento.getHoraText(), saida.getHoraText(), HoraType.ATRASO));
                            horasIndex++;
                        }
                    }
                }
            } 
        }
        
        return horasCalculadas;
    }
}